/*
    Copyright (C) 2013 Arno Rehn <arno@arnorehn.de>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "gslodemm.h"

using namespace arma;
using namespace GSL::ODE;

int main(int argc, char **argv)
{
    double mu = 10;
    System sys(2, [mu](double t, const vec& y, vec& f) {
        f[0] = y[1];
        f[1] = -y[0] - mu*y[1]*(y[0]*y[0] - 1);
    });

    Driver d(sys);
    vec t = linspace(0, 100, 101);
    vec y = { 1.0, 0.0 };
    mat out = d(t, y);

    std::cout << out << std::endl;
}
