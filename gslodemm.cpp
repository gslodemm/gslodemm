/*
    Copyright (C) 2013 Arno Rehn <arno@arnorehn.de>

    This file is part of gslodemm.

    gslodemm is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gslodemm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with gslodemm.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "gslodemm.h"

const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rk2 = gsl_odeiv2_step_rk2;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rk4 = gsl_odeiv2_step_rk4;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rkf45 = gsl_odeiv2_step_rkf45;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rkck = gsl_odeiv2_step_rkck;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rk8pd = gsl_odeiv2_step_rk8pd;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rk2imp = gsl_odeiv2_step_rk2imp;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rk4imp = gsl_odeiv2_step_rk4imp;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::bsimp = gsl_odeiv2_step_bsimp;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::rk1imp = gsl_odeiv2_step_rk1imp;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::msadams = gsl_odeiv2_step_msadams;
const gsl_odeiv2_step_type *GSL::ODE::Step::Type::msbdf = gsl_odeiv2_step_msbdf;

const decltype(&gsl_odeiv2_driver_alloc_y_new) GSL::ODE::Driver::Factory::YControl = &gsl_odeiv2_driver_alloc_y_new;
const decltype(&gsl_odeiv2_driver_alloc_yp_new) GSL::ODE::Driver::Factory::YPControl = &gsl_odeiv2_driver_alloc_yp_new;
const decltype(&gsl_odeiv2_driver_alloc_standard_new) GSL::ODE::Driver::Factory::StandardControl = &gsl_odeiv2_driver_alloc_standard_new;
const decltype(&gsl_odeiv2_driver_alloc_scaled_new) GSL::ODE::Driver::Factory::ScaledControl = &gsl_odeiv2_driver_alloc_scaled_new;
