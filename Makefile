all: libgslodemm.so

libgslodemm.so: gslodemm.cpp gslodemm.h
	g++ -std=c++11 -o libgslodemm.so -O3 -fPIC -shared `pkg-config --libs --cflags gsl` gslodemm.cpp

example: libgslodemm.so
	g++ -std=c++11 -o example -O3 -L . -lgslodemm `pkg-config --libs --cflags gsl` example.cpp

clean:
	rm -f example libgslodemm.so