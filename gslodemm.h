/*
    Copyright (C) 2013 Arno Rehn <arno@arnorehn.de>

    This file is part of gslodemm.

    gslodemm is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    gslodemm is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with gslodemm.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GSL_ODE_MM_H
#define GSL_ODE_MM_H

#include <functional>
#include <type_traits>
#include <armadillo>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>

namespace GSL {

class Exception : public std::exception {
public:
    const int errNo;

    Exception(int errNo) : errNo(errNo) {}
};

namespace ODE {

using namespace arma;

// Use the Curiously recurring template pattern (CRTP) for static polymorphism to avoid the indirection overhead of
// virtual functions.
// If the calls through std::function are too slow, subclass SystemBase and implement definingFunction() and jacobian().
template<typename DerivedSystem>
class SystemBase {
public:
    SystemBase(const SystemBase&) = delete;

    SystemBase(size_t dimension)
    {
        m_system.function = &functionWrapper;
        m_system.jacobian = &jacobianWrapper;
        m_system.dimension = dimension;
        m_system.params = this;
    }

    virtual ~SystemBase() {}

    inline operator gsl_odeiv2_system * () { return &m_system; }
    inline operator const gsl_odeiv2_system * () const { return &m_system; }

    inline gsl_odeiv2_system *gslSystem() { return *this; }
    inline const gsl_odeiv2_system *gslSystem() const { return *this; }

private:
    static int functionWrapper(double t, const double y[], double dydt[], void *params) {
        DerivedSystem *self = static_cast<DerivedSystem*>(params);
        vec vy(const_cast<double*>(y), self->m_system.dimension, false);
        vec vdydt(dydt, self->m_system.dimension, false);

        int errNo = GSL_SUCCESS;
        try {
            self->definingFunction(t, vy, vdydt);
        } catch (const ::GSL::Exception& e) {
            errNo = e.errNo;
        }
        return errNo;
    }

    static int jacobianWrapper(double t, const double y[], double *dfdy, double dfdt[], void *params) {
        DerivedSystem *self = static_cast<DerivedSystem*>(params);
        vec vy(const_cast<double*>(y), self->m_system.dimension, false);
        mat mdfdy(dfdy, self->m_system.dimension, self->m_system.dimension, false);
        vec vdfdt(dfdt, self->m_system.dimension, false);

        int errNo = GSL_SUCCESS;
        try {
            self->jacobian(t, vy, mdfdy, vdfdt);
        } catch (const ::GSL::Exception& e) {
            errNo = e.errNo;
        }

        // gsl uses row-major, armadillo column-major represenatation. maybe provide an overload that
        // skips transposing for speed reasons
        inplace_strans(mdfdy);

        return errNo;
    }

    gsl_odeiv2_system m_system;
};

class System : public SystemBase<System> {
public:
    typedef std::function<void(double t, const vec& y, vec& dydt)> DefiningFunction;
    typedef std::function<void(double t, const vec& y, mat& dfdy, vec& dfdt)> Jacobian;

    System(size_t dimension, const DefiningFunction& function, const Jacobian& jacobian = Jacobian())
        : SystemBase(dimension), m_function(function), m_jacobian(jacobian)
    {
    }

public:
    inline void definingFunction(double t, const vec& y, vec& dydt) {
        m_function(t, y, dydt);
    };
    inline void jacobian(double t, const vec& y, mat& dfdy, vec& dfdt) {
        m_jacobian(t, y, dfdy, dfdt);
    };

private:
    DefiningFunction m_function;
    Jacobian m_jacobian;
};

class Step {
public:
    struct Type {
        static const gsl_odeiv2_step_type *rk2;
        static const gsl_odeiv2_step_type *rk4;
        static const gsl_odeiv2_step_type *rkf45;
        static const gsl_odeiv2_step_type *rkck;
        static const gsl_odeiv2_step_type *rk8pd;
        static const gsl_odeiv2_step_type *rk2imp;
        static const gsl_odeiv2_step_type *rk4imp;
        static const gsl_odeiv2_step_type *bsimp;
        static const gsl_odeiv2_step_type *rk1imp;
        static const gsl_odeiv2_step_type *msadams;
        static const gsl_odeiv2_step_type *msbdf;
    };
};

class Driver {
public:
    struct Factory {
        static const decltype(&gsl_odeiv2_driver_alloc_y_new) YControl;
        static const decltype(&gsl_odeiv2_driver_alloc_yp_new) YPControl;
        static const decltype(&gsl_odeiv2_driver_alloc_standard_new) StandardControl;
        static const decltype(&gsl_odeiv2_driver_alloc_scaled_new) ScaledControl;
    };

    template<typename InitFunc, typename... Args>
    Driver(const InitFunc& init, const gsl_odeiv2_system *sys, const gsl_odeiv2_step_type *T, double hstart, Args... args) {
        m_driver = init(sys, T, hstart, args...);
    }

    Driver(const gsl_odeiv2_system *sys, const gsl_odeiv2_step_type *T, double hstart = 1e-6, double epsabs = 1e-6, double epsrel = 0) {
        m_driver = Factory::YControl(sys, T, hstart, epsabs, epsrel);
    }

    Driver(const gsl_odeiv2_system *sys, double hstart = 1e-6, double epsabs = 1e-6, double epsrel = 0) {
        m_driver = Factory::YControl(sys, Step::Type::rkf45, hstart, epsabs, epsrel);
    }

    Driver(const Driver&) = delete;

    ~Driver() {
        gsl_odeiv2_driver_free(m_driver);
    }

    inline void setStepSizeConstraints(double min, double max) {
        gsl_odeiv2_driver_set_hmin(m_driver, min);
        gsl_odeiv2_driver_set_hmax(m_driver, max);
    }

    inline void apply(double& t, double t1, double y[]) {
        int err = gsl_odeiv2_driver_apply(m_driver, &t, t1, y);
        if (err != GSL_SUCCESS) {
            throw ::GSL::Exception(err);
        }
    }

    template<class T>
    inline void apply(double& t, double t1, T& y) {
        apply(t, t1, &y[0]);
    }

    inline void apply(double& t, double h, unsigned long int n, double y[]) {
        int err = gsl_odeiv2_driver_apply_fixed_step(m_driver, &t, h, n, y);
        if (err != GSL_SUCCESS) {
            throw ::GSL::Exception(err);
        }
    }

    template<class T>
    inline void apply(double& t, double h, unsigned long int n, T& y) {
        apply(t, h, n, &y[0]);
    }

    mat operator()(const vec& t, const vec& y) {
        mat m(m_driver->sys->dimension + 1, t.n_elem);
        m(0, 0) = t(0);
        m(span(1, m.n_rows - 1), 0) = y;

        for (uword i = 1; i < t.n_elem; ++i) {
            // use previous values as starting values for new step
            m(span::all, i) = m(span::all, i-1);
            double *col = m.colptr(i);
            apply(col[0], t[i], col + 1);
        }

        return m.st();
    }

    inline void reset() { gsl_odeiv2_driver_reset(m_driver); }

protected:
    gsl_odeiv2_driver *m_driver;
};

}
}

#endif // GSL_ODE_MM_H
